# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :toot_rp,
       crawl_interval: 30,
       toot_visibility: "unlisted"

