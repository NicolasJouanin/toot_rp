defmodule TootRp.MixProject do
  use Mix.Project

  def project do
    [
      app: :toot_rp,
      version: "0.2.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      escript: [main_module: TootRp.MastoAuth],
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :ex_prompt],
      mod: {TootRp, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:hunter, path: "../hunter"},
      {:floki, "~> 0.21.0"},
      {:httpoison, "~> 1.5.1"},
      {:distillery, "~> 2.0"},
      {:ex_prompt, "~> 0.1.4"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
