defmodule TootRp do
  use Application

  @app "toot_rp"
  @home Path.expand(System.get_env("TOOT_RP_HOME") || "~/.toot_rp")
  @scopes ["read", "write", "follow"]

  def start(_type, _args) do

    try do
      read_token()
    rescue
      _ -> masto_init()
    end

    TootRp.Supervisor.start_link(name: TootRp.Supervisor)
  end

  defp masto_init() do
    IO.puts("This script generates the Mastodon application credentials for toot_rp.")
    instance_url=get_instance_url()
    user_name=get_user_name()
    password=get_password(user_name)

    try do
      app = Hunter.create_app(@app, "urn:ietf:wg:oauth:2.0:oob", @scopes, "https://toot_rp.org", [api_base_url: instance_url])
      IO.puts "#{@app} created on server #{instance_url}"
      client = Hunter.log_in(app, user_name, password, @scopes, instance_url)
      IO.puts "#{@app} logged in on server #{instance_url}"
      save_credentials(Map.merge(app, client))
    rescue
      e in Hunter.Error -> IO.puts("Application creation failed with reason: #{Hunter.Error.message(e)}")
    end
  end

  defp get_instance_url() do
    instance_url=ExPrompt.string(" Enter mastodon instance URL (defaults to https://mastodon.social/) : ")
    case URI.parse(instance_url) do
      %URI{host: nil} -> "https://mastodon.social/"
      _ -> String.trim(instance_url)
    end
  end

  defp get_user_name() do
    ExPrompt.string_required(" Enter mastodon login to use with this bot : ")
  end

  defp get_password(user) do
    ExPrompt.password(" Enter '#{user}' password : ")
  end

  defp save_credentials(app) do
    unless File.exists?(@home), do: File.mkdir_p!(@home)

    File.write!("#{@home}/app.json", Poison.encode!(app))
  end

  defp read_token() do
    File.read!("#{@home}/app.json")
    |> Poison.decode!()
  end

end
