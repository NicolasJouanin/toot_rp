defmodule TootRp.RpCrawler do
  use GenServer
  require Logger

  @default_crawl_interval Application.get_env(:toot_rp, :crawl_interval, 1)
  @rp_play_history "https://legacy.radioparadise.com/rp3.php?name=Music&file=play_history"

  def start_link(args) do
    GenServer.start_link(__MODULE__, %{crawl_interval: @default_crawl_interval}, args)
  end

  @impl true
  def init(state) do
    timer = set_timer(state)
    {:ok, Map.put(state, :timer, timer)}
  end

  @impl true
  def handle_call(:pop, _from, [head | tail]) do
    {:reply, head, tail}
  end

  @impl true
  def handle_cast(:crawl, state) do
    case HTTPoison.get(@rp_play_history) do
      {:ok, %{status_code: status, body: body}} when status in 200..299 ->
        [current_song|_] = parse_playlist(body)
        last_song = Map.get(state, :last_song, %{href: ""})
        if current_song[:href] != last_song[:href] do
          Logger.debug "New song playing \"#{current_song[:title]}\""
          #Cast change to RpTooter
          GenServer.cast(TootRp.RpTooter, {:toot_song, current_song})
          {:noreply, Map.put(state, :last_song, current_song)}
        else
          {:noreply, state}
        end
      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.warn "Radio Paradise playlist crawl failed. Reason: #{reason}}"
        {:noreply, state}
    end
  end

  @impl true
  def handle_info({:crawl_tick}, state) do
    GenServer.cast(self(), :crawl)
    timer = set_timer(state)
    {:noreply, Map.put(state, :timer, timer)}
  end

  def handle_info({:ssl_closed, _}, state) do
    Logger.warn("Unhandled ssl_closed message")
    {:noreply, state}
  end

  defp set_timer(%{crawl_interval: crawl_interval}) do
    Process.send_after(self(), {:crawl_tick}, crawl_interval * 1_000)
  end

  defp parse_playlist(body) do
      for {_, _, tds} <- Floki.find(body, "#content") |> Floki.find("tr") do
          parsed = for {_, attrs, content} <- tds do
            case {attrs,content} do
            {[{"title", _}], [time]} -> %{time: time}
            {[], [{"a",_,[{"img",[{"src",src},_,_],_}]},{"a",[{"href", href}], [title]}]} -> %{href: "https://www.radioparadise.com" <> href, img: src, title: title}
            {[{"style", "text-align:center;"}], [year]} -> %{year: year}
            {[{"title", "Average Rating"},{"class", _},{"style", "width:30px;text-align:center;"}], [rating]} -> %{rating: rating}
            _ -> %{}
            end
          end
          List.foldl(parsed, %{}, fn m1,acc -> Map.merge(acc,m1) end)
      end
      #Floki.find(body, "#content") |> Floki.find("tr")
  end

end