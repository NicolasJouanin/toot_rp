defmodule TootRp.RpTooter do
  use GenServer
  require Logger

  @home Path.expand(System.get_env("TOOT_RP_HOME") || "~/.toot_rp")
  @toot_visibility Application.get_env(:toot_rp, :toot_visibility, "unlisted")

  def start_link(_args) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  @impl true
  def init(state) do
    creds = read_token()
    client = Hunter.Client.new([base_url: creds["base_url"], bearer_token: creds["bearer_token"]])
    {:ok, Map.merge(state, creds) |> Map.put(:client, client)}
  end

  @impl true
  def handle_cast({:toot_song, song}, state) do
    Logger.debug "Toot new song '#{song[:title]}' with visibility #{@toot_visibility}"
    Hunter.Status.create_status(state[:client], format_song(song), [visibility: @toot_visibility])
    {:noreply, state}
  end

  defp format_song(song) do
    "#{song[:title]}\n\u{1F4C5}: #{song[:year]}  \u2B50: #{song[:rating]}\n#{format_hashtag(song[:title])}\n#{song[:href]}"
  end

  defp read_token() do
    File.read!("#{@home}/app.json")
    |> Poison.decode!()
  end

  defp format_hashtag(song_title) do
    [author|_] = String.split(song_title, "—")
    "#" <> (author |> String.split(" ") |> Enum.map( fn word -> String.trim(word) |> String.capitalize  end) |> Enum.join(""))
  end

end